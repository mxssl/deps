# deps

## How to run

1. Install [Go](https://golang.org/doc/install)
2. Create dir `mkdir -p ${GOPATH}/src/gitlab.com/mxssl/deps`
3. `cd ${GOPATH}/src/gitlab.com/mxssl/deps`
4. `git clone git@gitlab.com:mxssl/deps.git .`
5. `go run main.go`
6. Run unit tests

```
go test -v ./...
=== RUN   TestCase1
--- PASS: TestCase1 (0.00s)
=== RUN   TestCase2
--- PASS: TestCase2 (0.00s)
=== RUN   TestCase3
--- PASS: TestCase3 (0.00s)
PASS
ok  	gitlab.com/mxssl/deps	0.005s
```
